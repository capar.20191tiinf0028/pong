#include <stdio.h>
#include <stdlib.h>
#include <SDL2/sdl.h>

#define LARGURA 640
#define ALTURA 480

SDL_Rect entidades[3] = {
	(SDL_Rect) {0, 480 / 2, 20, 50},
	(SDL_Rect) {LARGURA - 20, 480 / 2, 20, 50},
	(SDL_Rect) {LARGURA / 2, ALTURA / 2, 20, 20}
};

#define TOTAL_ENTIDADES 3

void printar_erro();
void desenhar_entidades(SDL_Renderer *renderizador);

int main(int argc, char *argv[]) {
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		printar_erro();
		goto limpar;
	}

	SDL_Window *janela = NULL;
	SDL_Renderer *renderizador = NULL;

	if (SDL_CreateWindowAndRenderer(
		LARGURA,
		ALTURA,
		SDL_WINDOW_SHOWN,
		&janela,
		&renderizador
	) == -1) {
		printar_erro();
		goto limpar;
	}


	int jogoAcabou = 0;
	
	SDL_Event evento;

	while (jogoAcabou == 0) {
		while (SDL_PollEvent(&evento) == 1) {
			switch(evento.type){
				case SDL_QUIT:
					jogoAcabou = 1;
					break;
				default:
					break;
			}
		}

		SDL_RenderClear(renderizador);

		desenhar_entidades(renderizador);

		SDL_RenderPresent(renderizador);
	}

limpar:
	if (janela != NULL) SDL_DestroyWindow(janela);
	if (renderizador != NULL) SDL_DestroyRenderer(renderizador);
	SDL_Quit();
	exit(0);
}

void printar_erro() {
	printf("Um erro occoreu!\n");
	printf("Erro: %s\n", SDL_GetError());
}

void desenhar_entidades(SDL_Renderer *renderizador) {
	SDL_SetRenderDrawColor(
		renderizador,
		255,
		255,
		255,
		255
	);

	SDL_RenderFillRects(renderizador, entidades, TOTAL_ENTIDADES);

	SDL_SetRenderDrawColor(
		renderizador,
		0,
		0,
		0,
		255
	);
}
